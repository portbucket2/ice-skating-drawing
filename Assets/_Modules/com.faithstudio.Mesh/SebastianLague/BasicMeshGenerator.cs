﻿namespace com.faithstudio.Mesh
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    #region Custom Variables

    public enum Shape
    {
        Plane,
        Cube,
        Sphere
    }

    #endregion

    public class BasicMeshGenerator : MonoBehaviour
    {

        #region PublicVariables

        public Shape shapeOfMesh = Shape.Cube;
        [Range(2, 256)]
        public int resolution;

        #endregion

        #region Private Variables

        private int         m_NumberOfMeshFace;
        private int[]       m_NumberOfResolutionOnMeshFace;
        private Vector3[]   m_DirectionOfMeshFace;
        private Vector3[]   m_DisplacementOfMeshFace;

        [SerializeField,HideInInspector]
        private List<MeshFilter>    m_ListOfMeshFilters;
        private MeshFace[]          m_TerrainFace;
        #endregion

        #region Mono Behaviour

        private void OnValidate()
        {


            Initialize();
            GenerateMesh();
        }

        #endregion

        #region Configuretion

        private bool IsValidMeshFilterIndex(int t_Index) {

            if (t_Index >= 0 && t_Index < m_ListOfMeshFilters.Count) {

                return true;
            }

            return false;
        }

        private void Initialize()
        {
            if (m_ListOfMeshFilters == null)
                m_ListOfMeshFilters = new List<MeshFilter>();

            switch (shapeOfMesh)
            {
                case Shape.Plane:
                    m_NumberOfMeshFace = 1;
                    m_NumberOfResolutionOnMeshFace = new int[] { resolution };
                    m_DirectionOfMeshFace = new Vector3[] { Vector3.up};
                    m_DisplacementOfMeshFace = new Vector3[] { Vector3.zero };
                    break;
                case Shape.Cube:
                    m_NumberOfMeshFace = 6;
                    m_NumberOfResolutionOnMeshFace = new int[] { resolution, resolution, resolution, resolution, resolution, resolution };
                    m_DirectionOfMeshFace = new Vector3[] { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };
                    m_DisplacementOfMeshFace = m_DirectionOfMeshFace;
                    break;
                case Shape.Sphere:
                    m_NumberOfMeshFace = 6;
                    m_NumberOfResolutionOnMeshFace = new int[] { resolution, resolution, resolution, resolution, resolution, resolution };
                    m_DirectionOfMeshFace = new Vector3[] { Vector3.up, Vector3.down, Vector3.left, Vector3.right, Vector3.forward, Vector3.back };
                    m_DisplacementOfMeshFace = m_DirectionOfMeshFace;
                    break;
            }

            int t_NumberOfOldMeshFilterer = m_ListOfMeshFilters.Count;
            if (m_NumberOfMeshFace >= m_ListOfMeshFilters.Count)
            {
                for (int meshFiltererIndex = 0; meshFiltererIndex < m_NumberOfMeshFace; meshFiltererIndex++)
                {

                    if (meshFiltererIndex >= t_NumberOfOldMeshFilterer)
                    {

                        GameObject t_MeshObject = new GameObject("Mesh");
                        t_MeshObject.transform.parent = transform;

                        t_MeshObject.AddComponent<MeshRenderer>().sharedMaterial = new Material(Shader.Find("Standard"));
                        m_ListOfMeshFilters.Add(t_MeshObject.AddComponent<MeshFilter>());
                        m_ListOfMeshFilters[meshFiltererIndex].sharedMesh = new Mesh();
                    }

                    m_ListOfMeshFilters[meshFiltererIndex].gameObject.name = "Mesh " + m_DirectionOfMeshFace[meshFiltererIndex];

                    if(!m_ListOfMeshFilters[meshFiltererIndex].gameObject.activeInHierarchy)
                        m_ListOfMeshFilters[meshFiltererIndex].gameObject.SetActive(true);
                }
            }
            else {

                for (int meshFiltererIndex = 0; meshFiltererIndex < t_NumberOfOldMeshFilterer; meshFiltererIndex++) {

                    if (meshFiltererIndex >= m_NumberOfMeshFace) {

                        m_ListOfMeshFilters[meshFiltererIndex].gameObject.SetActive(false);
                    }
                }
            }

            m_TerrainFace = new MeshFace[m_NumberOfMeshFace];
            for (int i = 0; i < m_NumberOfMeshFace; i++)
            {

                m_TerrainFace[i] = new MeshFace(
                        shapeOfMesh,
                        m_ListOfMeshFilters[i].sharedMesh,
                        m_NumberOfResolutionOnMeshFace[i],
                        m_DirectionOfMeshFace[i],
                        m_DisplacementOfMeshFace[i]
                    );
            }
        }

        private void GenerateMesh()
        {
            foreach (MeshFace t_TerrainFace in m_TerrainFace)
            {

                t_TerrainFace.ConstructMesh();
            }
        }

        public MeshFilter GetMeshFilter(int t_Index = 0) {

            return m_ListOfMeshFilters[t_Index];
        }

        public DeformedVertexPoint[] GetArraysOfDeformedVertexPoint(int t_Index = 0) {

            return m_TerrainFace[t_Index].GetDeformedVertexPoint();
        }

        public float GetErrorRateOfVertexDisplacement(int t_Index = 0) {

            Vector3 t_LocalScale = m_ListOfMeshFilters[t_Index].transform.localScale;
            float t_Scale = (t_LocalScale.x + t_LocalScale.y + t_LocalScale.z) / 3.0f;
            t_Scale /= (resolution * 0.5f);
            //return 0;
            return t_Scale;
        }

        #endregion
    }

}

