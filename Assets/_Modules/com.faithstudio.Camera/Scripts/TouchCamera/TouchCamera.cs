﻿namespace com.faithstudio.Camera
{
    using UnityEngine;

    using System.Collections.Generic;

    using com.faithstudio.Math;
    using com.faithstudio.Gameplay;

    public class TouchCamera : MonoBehaviour
    {
        #region Public Variables

        public Transform verticalMark;
        public Transform horizontalMark;

        [Space(5.0f)]
        public Transform cameraTransformReference;
        public List<Transform> focusPoint;
        [Range(0f, 1f)]
        public float forwardVelocityOfCamera;

        [Space(5.0f)]
        public LerpTouchController lerpTouchControllerReference;

        [Header("Parameter  :   CameraFocus")]
        public CameraFocus cameraFocusReference;
        [Range(-1f,1f)]
        public float cameraFocusValue = 0.1f;
        [Range(0f,1f)]
        public float focusSpeed = 0.75f;
        [Range(0f,1f)]
        public float unfocusSpeed = 0.825f;

        #endregion

        #region Private Variables

        private bool m_IsTouchCameraControllerRunning;

        private List<Transform> m_CameraFocusingObject;

        private Vector3 m_TargetedPosition;
        private Vector3 m_ModifiedPosition;

        #endregion

        #region Mono Behaviour

        private void LateUpdate()
        {
            if (m_IsTouchCameraControllerRunning)
            {

                m_TargetedPosition = new Vector3(
                        horizontalMark.position.x,
                        verticalMark.position.y,
                        (horizontalMark.position.z + verticalMark.position.z) / 2.0f
                    );
                m_ModifiedPosition = Vector3.Lerp(
                        cameraTransformReference.position,
                        m_TargetedPosition,
                        forwardVelocityOfCamera
                    );
                cameraTransformReference.position = m_ModifiedPosition;

            }
        }

        #endregion

        #region Configuretion

        private void OnTouchDown(Vector3 t_TouchPosition)
        {
            cameraFocusReference.FocusCamera(
                    m_CameraFocusingObject,
                    Vector3.zero,
                    cameraFocusValue,
                    focusSpeed,
                    unfocusSpeed
                );
        }

        private void OnTouchUp(Vector3 t_TouchPosition)
        {
            cameraFocusReference.UnfocusCamera();
        }

        #endregion

        #region Public Callback

        public void PreProcess(List<Transform> t_CameraFocusingObject = null)
        {

            if (t_CameraFocusingObject == null)
                m_CameraFocusingObject = focusPoint ;
            else
                m_CameraFocusingObject = t_CameraFocusingObject;


            m_IsTouchCameraControllerRunning = true;
            lerpTouchControllerReference.EnableLerpTouchController(true);
            GlobalTouchController.Instance.OnTouchDown += OnTouchDown;
            GlobalTouchController.Instance.OnTouchUp += OnTouchUp;
        }

        public void PostProcess()
        {

            m_IsTouchCameraControllerRunning = false;
            lerpTouchControllerReference.DisableLerpTouchController();
            GlobalTouchController.Instance.OnTouchDown -= OnTouchDown;
            GlobalTouchController.Instance.OnTouchUp -= OnTouchUp;
        }

        #endregion

    }
}

