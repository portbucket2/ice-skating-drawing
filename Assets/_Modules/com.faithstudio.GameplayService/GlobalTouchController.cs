﻿namespace com.faithstudio.Gameplay
{
    using UnityEngine;

    public class GlobalTouchController : MonoBehaviour
    {

        #region Custom Variables

        public delegate void OnTouchDownEvent(Vector3 t_TouchPosition);
        public delegate void OnTouchEvent(Vector3 t_TouchPosition);
        public delegate void OnTouchUpEvent(Vector3 t_TouchPosition);

        #endregion

        #region Public Variables

        public static GlobalTouchController Instance;

        public OnTouchDownEvent OnTouchDown;
        public OnTouchEvent OnTouch;
        public OnTouchUpEvent OnTouchUp;

        public bool enableGlobalTouchControllerOnAwake;

        #endregion

        #region Private Variables

        private bool m_IsTouchControllerRunning = false;

        private Vector2 m_LatestTouchPosition;
        private Vector2 m_ScreenResolution;

        private Touch m_ActiveTouch;

        #endregion

        #region Mono Bheaviour

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {

                Destroy(gameObject);
            }

            if(enableGlobalTouchControllerOnAwake)
                EnableTouchController();
        }

        private void Update()
        {
            if (m_IsTouchControllerRunning)
            {
                TouchController();
            }
        }

        #endregion

        #region Configuretion

        private void TouchController()
        {

#if UNITY_EDITOR

            if (Input.GetMouseButtonDown(0))
            {
                m_LatestTouchPosition = Input.mousePosition;
                OnTouchDown?.Invoke(m_LatestTouchPosition);
            }

            if (Input.GetMouseButton(0))
            {
                m_LatestTouchPosition = Input.mousePosition;
                OnTouch?.Invoke(m_LatestTouchPosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                m_LatestTouchPosition = Input.mousePosition;
                OnTouchUp?.Invoke(m_LatestTouchPosition);
            }


#elif UNITY_ANDROID || UNITY_IOS
       if (Input.touchCount > 0) {

                m_ActiveTouch           = Input.GetTouch(0);
                m_LatestTouchPosition   = m_ActiveTouch.position;
                switch (m_ActiveTouch.phase)
                {

                    case TouchPhase.Began:
                        OnTouchDown?.Invoke(m_LatestTouchPosition);
                        break;

                    case TouchPhase.Stationary:
                        OnTouch?.Invoke(m_LatestTouchPosition);
                        break;

                    case TouchPhase.Moved:
                        OnTouch?.Invoke(m_LatestTouchPosition);
                        break;

                    case TouchPhase.Ended:
                        OnTouchUp?.Invoke(m_LatestTouchPosition);
                        break;

                    case TouchPhase.Canceled:
                        OnTouchUp?.Invoke(m_LatestTouchPosition);
                        break;
                }
            }
#endif

        }

        #endregion

        #region Public Callback

        public void EnableTouchController()
        {
            m_ScreenResolution = new Vector2(
                Screen.width,
                Screen.height
            );
            m_IsTouchControllerRunning = true;
        }

        public void DisableTouchController(bool t_ResetTouchEvents = false)
        {

            m_IsTouchControllerRunning = false;

            if (t_ResetTouchEvents)
            {

                OnTouchDown = null;
                OnTouch = null;
                OnTouchUp = null;
            }
        }

        public float GetInterpolatedValueForHorizontal(bool t_Clamp01 = true){

            if(t_Clamp01)
                return m_LatestTouchPosition.x / m_ScreenResolution.x;
            else
                return (-1 + (m_LatestTouchPosition.x / m_ScreenResolution.x) * 2f);
        }

        public float GetInterpolatedValueForVertical(bool t_Clamp01 = true){

            if(t_Clamp01)
                return m_LatestTouchPosition.y / m_ScreenResolution.y;
            else
                return (-1 + (m_LatestTouchPosition.y / m_ScreenResolution.y) * 2f);
        }

        #endregion
    }
}


