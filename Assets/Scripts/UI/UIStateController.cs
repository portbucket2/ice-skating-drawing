﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public class UIStateController : MonoBehaviour
{

    #region Public Variables

    public static UIStateController Instnace;

    public UIMainMenuController UIMainMenuControllerReference;

    #endregion

    #region Mono Behaviour

    private void Awake(){

        Instnace = this;
        UIMainMenuControllerReference.AppearMainMenu();
    }

    #endregion

}
