﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIMainMenuController : MonoBehaviour
{
    #region Public Variables

    public Animator animatorReferenceForMainMenu;

    [Space(5.0f)]
    public Button startButton;

    #endregion

    #region Private Variables

    private bool m_IsMainMenuShowing = false;

    #endregion

    #region  Mono Behaviour

    private void Awake() {
        
        startButton.onClick.AddListener(delegate{

            DisappearMainMenu();
            GameplayController.Instance.PreProcess();
        });
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForAppearMainMenu(){

        animatorReferenceForMainMenu.SetTrigger("APPEAR");
        yield return null;
    }

    private IEnumerator ControllerForDisappearMainMenu(){

        animatorReferenceForMainMenu.SetTrigger("DISAPPEAR");
        yield return null;
    }

    #endregion

    #region Public Callback

    public void AppearMainMenu(){

        if(!m_IsMainMenuShowing){

            m_IsMainMenuShowing = true;
            StartCoroutine(ControllerForAppearMainMenu());
        }
    }

    public void DisappearMainMenu(){

        if(m_IsMainMenuShowing){

            m_IsMainMenuShowing = false;
            StartCoroutine(ControllerForDisappearMainMenu());
        }
    }

    #endregion
}
