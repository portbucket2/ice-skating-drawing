﻿using UnityEngine;

public class CharacterFactory : MonoBehaviour
{
    #region Custom Variable

    [System.Serializable]
    public struct Character
    {
        public Animator         animatorReference;
        public TrailRenderer[]  trailRendererReferences;
    }

    #endregion

    #region Public Variables

    public string characterTag;
    public Character[] characterData;

    #endregion 

    #region Private Variables

    #endregion


    #region Configuretion

    private string GetPlayerPrefKeyForThisCharacter(){

        return "CHAR_TAG_" + characterTag;
    }

    private bool IsValidCharacterIndex(int t_CharacterIndex){

        if(t_CharacterIndex >= 0 && t_CharacterIndex < characterData.Length)
            return true;

        Debug.LogError("Invalid Character Index, must be within [0," + characterData.Length + ")");
        return false;
    }

    #endregion

    #region  Public Callback

    public int GetIndexForSelectedCharacter(){

        return PlayerPrefs.GetInt(GetPlayerPrefKeyForThisCharacter() + "_SC", 0);
    }

    public Animator GetAnimatorForSelectedCharacter(){

        return characterData[GetIndexForSelectedCharacter()].animatorReference;
    }

    public void SetIndexForSelectedCharacter(int t_CharacterIndex){

        if(IsValidCharacterIndex(t_CharacterIndex))
            PlayerPrefs.SetInt(GetPlayerPrefKeyForThisCharacter() + "_SC",t_CharacterIndex);
    }

    public void GoToNextCharacter(){

        int t_SelectedIndex = GetIndexForSelectedCharacter() + 1;
        if(IsValidCharacterIndex(t_SelectedIndex))
            SetIndexForSelectedCharacter(t_SelectedIndex);
        else
            SetIndexForSelectedCharacter(0);

        ActivateSelectedCharacter();
    }

    public void GoToPreviousCharacter(){

        int t_SelectedIndex = GetIndexForSelectedCharacter() - 1;
        if(IsValidCharacterIndex(t_SelectedIndex))
            SetIndexForSelectedCharacter(t_SelectedIndex);
        else
            SetIndexForSelectedCharacter(characterData.Length);

        ActivateSelectedCharacter();
    }

    public void ActivateSelectedCharacter(){

        int t_SelectedIndex = GetIndexForSelectedCharacter();
        int t_NumberOfAvailableCharacter = characterData.Length;
        for(int i = 0; i < t_NumberOfAvailableCharacter; i++){

            int t_NumberOfTrailRenderer = characterData[i].trailRendererReferences.Length;

            if(t_SelectedIndex == i){
                
                characterData[i].animatorReference.gameObject.SetActive(true);
                for(int j = 0 ; j < t_NumberOfTrailRenderer; j++){
                    characterData[i].trailRendererReferences[j].enabled = true;
                }
                
            }else{

                characterData[i].animatorReference.gameObject.SetActive(false);
                for(int j = 0 ; j < t_NumberOfTrailRenderer; j++){
                    characterData[i].trailRendererReferences[j].enabled = false;
                }
            }
        }
    }

    #endregion
}
