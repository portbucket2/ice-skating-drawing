﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CharacterFactory))]
public class CharacterFactoryEditor : Editor
{
    private CharacterFactory Reference;

    private void OnEnable() {
        
        //Reference = (CharacterFactory) target;
        
    }

    public override void OnInspectorGUI(){

        serializedObject.Update();

        CustomGUI();
        base.OnInspectorGUI();

        serializedObject.ApplyModifiedProperties();
    }

    private void CustomGUI(){

        if(Reference.characterTag.Length == 0){
            Reference.characterTag = Reference.gameObject.name;
        }

        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.LabelField("SelectedCharacter(" + Reference.GetIndexForSelectedCharacter() + ") : ");
            if(GUILayout.Button("Reset")){

                Reference.SetIndexForSelectedCharacter(0);
            }
            if(GUILayout.Button("Prev")){
                
                Reference.GoToPreviousCharacter();
            }
            if(GUILayout.Button("Next")){
                
                Reference.GoToNextCharacter();
            }
            
        }
        EditorGUILayout.EndHorizontal();
    }
}
