﻿using System.Collections;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
    #region Public Variables

    public static GameplayController Instance;

    [Header("Reference      :   External")]
    public PlayerController playerControllerReference;

    #endregion

    #region Private Variables

    private bool m_IsGameRunning;

    #endregion

    #region  Mono Behaviour

    private void Awake() {
        
        Instance = this;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForPreProcess(){

        playerControllerReference.PreProcess();
        yield return null;
    }

    private IEnumerator ControllerForPostProcess(){

        playerControllerReference.PostProcess();
        yield return null;
    }

    #endregion

    #region Public Callback

    public void PreProcess(){

        if(!m_IsGameRunning){

            m_IsGameRunning = true;
            StartCoroutine(ControllerForPreProcess());
        }
    }

    public void PostProcess(){

        if(m_IsGameRunning){

            m_IsGameRunning = false;
            StartCoroutine(ControllerForPostProcess());
        }
    }

    #endregion
}
