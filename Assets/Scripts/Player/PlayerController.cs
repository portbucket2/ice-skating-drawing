﻿using System.Collections;

using com.faithstudio.Camera;
using com.faithstudio.Gameplay;

using com.alphapotato.Gameplay;

using UnityEngine;
using UnityEngine.Events;

public class PlayerController : ExtendedMonoBehaviour {
    
    #region Public Variables

    #if UNITY_EDITOR

    [HideInInspector]
    public bool showCharacterFactoryOnEditor;

    #endif

    [Header ("Reference  :   External")]
    public TouchCamera  touchCameraReference;
    public Transform    playerTransformReference;

    [Header ("Parameter  :   Configuretion - ForwardVelocity")]
    [Range (1f, 5f)]
    public float durationForReachingMaximumForwardVelocity = 1f;
    public AnimationCurve accelerationCurveForForwardVelocity = new AnimationCurve (new Keyframe[] { new Keyframe (0f, 0f), new Keyframe (1f, 1f) });

    [Space (5.0f)]
    public float durationForReachingZeroForwardVelocity = 3;
    public AnimationCurve decelerationCurveForForwardVelocity = new AnimationCurve (new Keyframe[] { new Keyframe (0f, 1f), new Keyframe (1f, 0f) });

    [Header ("Parameter  :   DefaultValues")]
    [Range (0f, 100f)]
    public float defaultForwardVelocity = 1.0f;
    [Range (0f, 100f)]
    public float defaultAngulerVelocity = 0.1f;

    public CharacterFactory characterFactory;

    #endregion

    #region  Private Variables

    private TimeController m_TimeControllerReference;
    private GlobalTouchController m_GlobalTouchControllerReference;

    private bool m_IsTouchInputEnabled;

    private float m_CurrentForwardVelocity;
    private float m_AbsoluteForwardVelocity;

    private float m_AbsoluteAngulerVelocity;

    private Vector3 m_CalculatedForwardVelocity;

    private Animator m_SelectedAnimator;

    private UnityAction<float> OnUpdatingSpeedProgression;

    #endregion

    #region Configuretion   :   Touch Input

    private void OnTouchDown (Vector3 t_TouchPosition) {

        if (!m_IsTouchInputEnabled) {

            m_SelectedAnimator = characterFactory.GetAnimatorForSelectedCharacter();
            m_SelectedAnimator.SetTrigger("RUN");
            m_IsTouchInputEnabled = true;
            StartCoroutine (ControllerForAcceleration ());
            EnableMonoBehaviour ();
        }
    }

    private void OnTouch (Vector3 t_TouchPosition) {

    }

    private void OnTouchUp (Vector3 t_TouchPosition) {

        if (m_IsTouchInputEnabled) {
            
            m_SelectedAnimator.SetTrigger("IDLE");
            m_IsTouchInputEnabled = false;
            StartCoroutine (ControllerForDeceleration ());
            
        }
    }

    #endregion

    #region Configuretion   :   Acceleration & Decelration

    private IEnumerator ControllerForAcceleration () {

        float t_CycleLength = 0.0167f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime (t_CycleLength);

        float t_Progression = 0;
        float t_RemainingTimeForAcceleration = durationForReachingMaximumForwardVelocity;
        t_RemainingTimeForAcceleration *= (1f - GetProgressionOfForwardVelocity());

        while (m_IsTouchInputEnabled && t_RemainingTimeForAcceleration > 0) {

            t_Progression = accelerationCurveForForwardVelocity.Evaluate (1f - (t_RemainingTimeForAcceleration / durationForReachingMaximumForwardVelocity));
            m_CurrentForwardVelocity = t_Progression * m_AbsoluteForwardVelocity;
            
            OnUpdatingSpeedProgression?.Invoke(GetProgressionOfForwardVelocity());

            yield return t_CycleDelay;
            t_RemainingTimeForAcceleration -= t_CycleLength;
        }

        if (t_RemainingTimeForAcceleration <= 0)
            m_CurrentForwardVelocity = m_AbsoluteForwardVelocity;

        StopCoroutine (ControllerForAcceleration ());

    }

    private IEnumerator ControllerForDeceleration () {

        float t_CycleLength = 0.0167f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime (t_CycleLength);

        float t_Progression = 0;
        float t_RemainingTimeForDeceleration = durationForReachingZeroForwardVelocity;
        t_RemainingTimeForDeceleration *= GetProgressionOfForwardVelocity();

        while (!m_IsTouchInputEnabled && t_RemainingTimeForDeceleration > 0) {

            t_Progression = decelerationCurveForForwardVelocity.Evaluate (1f - (t_RemainingTimeForDeceleration / durationForReachingZeroForwardVelocity));
            m_CurrentForwardVelocity = t_Progression * m_AbsoluteForwardVelocity;

            OnUpdatingSpeedProgression?.Invoke(GetProgressionOfForwardVelocity());

            yield return t_CycleDelay;
            t_RemainingTimeForDeceleration -= t_CycleLength;
        }

        StopCoroutine (ControllerForDeceleration ());
    }

    private void OnUpdateMethodForForwardMovement () {

        playerTransformReference.Translate(
            Vector3.forward * m_CurrentForwardVelocity * m_TimeControllerReference.GetAbsoluteDeltaTime()
        );

        m_SelectedAnimator.speed = 0.5f + (0.5f * GetProgressionOfForwardVelocity());
    }

    private void OnUpdateMethodForAngulerMovement(){

        if(m_IsTouchInputEnabled){
            Vector3 t_ModifiedRotation = Vector3.up * m_AbsoluteAngulerVelocity * m_TimeControllerReference.GetAbsoluteDeltaTime() * m_GlobalTouchControllerReference.GetInterpolatedValueForHorizontal(false);
            playerTransformReference.Rotate(t_ModifiedRotation);
        }
       
    }

    #endregion

    #region Public Callback

    public void PreProcess (
        float t_ForwardVelocity = 0f,
        float t_AngulerVelocity = 0f,
        UnityAction<float> OnUpdatingSpeedProgression = null) {

        if (t_ForwardVelocity == 0)
            m_AbsoluteForwardVelocity = defaultForwardVelocity;
        else
            m_AbsoluteForwardVelocity = t_ForwardVelocity;

        if (t_AngulerVelocity == 0)
            m_AbsoluteAngulerVelocity = defaultAngulerVelocity;
        else
            m_AbsoluteAngulerVelocity = t_AngulerVelocity;

        this.OnUpdatingSpeedProgression = OnUpdatingSpeedProgression;

        m_TimeControllerReference       = TimeController.Instance;
        m_GlobalTouchControllerReference= GlobalTouchController.Instance;

        m_GlobalTouchControllerReference.OnTouchDown += OnTouchDown;
        m_GlobalTouchControllerReference.OnTouchUp += OnTouchUp;
        base.OnUpdateEvent += OnUpdateMethodForForwardMovement;
        base.OnUpdateEvent += OnUpdateMethodForAngulerMovement;

        touchCameraReference.PreProcess();
    }

    public void PostProcess () {

        touchCameraReference.PostProcess();
        DisableMonoBeheaviour ();

        m_GlobalTouchControllerReference.OnTouchDown -= OnTouchDown;
        m_GlobalTouchControllerReference.OnTouchUp -= OnTouchUp;
        base.OnUpdateEvent -= OnUpdateMethodForForwardMovement;
        base.OnUpdateEvent -= OnUpdateMethodForAngulerMovement;
    }

    public float GetProgressionOfForwardVelocity () {

        return (m_CurrentForwardVelocity / m_AbsoluteForwardVelocity);
    }

    #endregion
}