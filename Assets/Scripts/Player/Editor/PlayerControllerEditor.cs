﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (PlayerController))]
public class PlayerControllerEditor : Editor {
    private PlayerController    Reference;
    private Editor              characterFactoryEditor;

    private void OnEnable () {

        Reference = (PlayerController) target;
    }

    public override void OnInspectorGUI () {

        serializedObject.Update ();

        if (EditorApplication.isPlaying) {

            if (GUILayout.Button ("Enable Player")) {

                Reference.PreProcess ();
            }
        }

        base.OnInspectorGUI();

        EditorGUILayout.Space();
        DrawSettingsEditor(Reference.characterFactory, null, ref Reference.showCharacterFactoryOnEditor, ref characterFactoryEditor);

        serializedObject.ApplyModifiedProperties ();
    }

    private void DrawSettingsEditor(Object settings, System.Action OnSettingsUpdated, ref bool foldout, ref Editor editor) {

        if (settings != null) {

            using (var check = new EditorGUI.ChangeCheckScope())
            {

                foldout = EditorGUILayout.InspectorTitlebar(foldout, settings);

                if (foldout)
                {

                    CreateCachedEditor(settings, null, ref editor);
                    editor.OnInspectorGUI();

                    if (check.changed)
                    {

                        if (OnSettingsUpdated != null)
                        {

                            OnSettingsUpdated.Invoke();
                        }
                    }
                }
            }
        }
    }
}